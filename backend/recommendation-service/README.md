# Read Me First

Spring application with Restful API. Runs on port: 8080.

Exposed secured (for "http://localhost:4200") endpoints:

* /api/products - for GET request that returns a JSON representation of the recommended products for given answers to
  the questions.
* /api/product - for POST request to create new product resource that returns created product if the product was created
  successfully, or the message if the resource already exists in DB.

### How to run

* IDE - run RecommendationServiceApplication.main() method.
* Using Maven plugin - navigate to project root and execute the command: mvn spring-boot:run.

### TODO

* /api/product needs validation that age and income min values would not be higher than max values.