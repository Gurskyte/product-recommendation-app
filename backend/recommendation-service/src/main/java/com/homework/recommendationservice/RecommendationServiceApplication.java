package com.homework.recommendationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
public class RecommendationServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(RecommendationServiceApplication.class, args);
	}
}