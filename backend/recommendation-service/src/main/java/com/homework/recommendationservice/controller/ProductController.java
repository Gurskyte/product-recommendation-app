package com.homework.recommendationservice.controller;

import com.homework.recommendationservice.entity.Product;
import com.homework.recommendationservice.exception.EntityAlreadyExists;
import com.homework.recommendationservice.service.ProductSearchCriteriaBuilder;
import com.homework.recommendationservice.service.ProductService;
import com.homework.recommendationservice.service.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductSearchCriteriaBuilder criteriaBuilder;

    @GetMapping(value = "/products")
    public ResponseEntity<ProductsResponse> getProducts(@RequestParam(value = "search", required = false) String search) {
        List<SearchCriteria> searchCriteria = criteriaBuilder.build(search);
        List<Product> products = productService.getProducts(searchCriteria);
        return ResponseEntity.ok(new ProductsResponse(products));
    }

    @PostMapping(value = "/product")
    public ResponseEntity<?> saveProduct(@Valid @RequestBody Product product) {
        try {
            Product savedProduct = productService.saveProduct(product);
            return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
        } catch (EntityAlreadyExists entityAlreadyExists) {
            return new ResponseEntity<>("Product already exists.", HttpStatus.CONFLICT);
        }
    }
}