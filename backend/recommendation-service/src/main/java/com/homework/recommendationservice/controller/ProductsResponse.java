package com.homework.recommendationservice.controller;

import com.homework.recommendationservice.entity.Product;

import java.util.List;

public class ProductsResponse {
    private final List<Product> products;

    public ProductsResponse(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }
}