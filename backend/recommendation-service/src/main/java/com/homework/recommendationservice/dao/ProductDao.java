package com.homework.recommendationservice.dao;


import com.homework.recommendationservice.entity.Product;
import com.homework.recommendationservice.exception.EntityAlreadyExists;
import com.homework.recommendationservice.service.ProductSearchQueryCriteriaConsumer;
import com.homework.recommendationservice.service.SearchCriteria;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class ProductDao {

    @PersistenceContext
    private EntityManager entityManager;

    private final static Logger LOGGER = Logger.getLogger(ProductDao.class.getName());

    public List<Product> searchProduct(List<SearchCriteria> params) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> query = builder.createQuery(Product.class);
        Root<Product> r = query.from(Product.class);

        Predicate predicate = builder.conjunction();

        ProductSearchQueryCriteriaConsumer searchConsumer =
                new ProductSearchQueryCriteriaConsumer(predicate, builder, r);

        List<Predicate> predicateList = new ArrayList<>();
        for (SearchCriteria param : params) {
            searchConsumer.accept(param);
            predicateList.add(searchConsumer.getPredicate());
        }
        query.where(builder.and(predicateList.toArray(Predicate[]::new)));

        if (predicate != null) {
            return entityManager.createQuery(query).getResultList();
        }
        return Collections.emptyList();
    }

    @Transactional
    public Product saveProduct(Product product) throws EntityAlreadyExists {
        try {
            entityManager.persist(product);
            entityManager.flush();
            return product;
        } catch (PersistenceException e) {
            LOGGER.log(Level.INFO, "Failed to contain unique constraint rules", e);
            throw new EntityAlreadyExists("Product violates constraint rules: " + product, e);
        }
    }
}
