package com.homework.recommendationservice.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "PRODUCT", uniqueConstraints = @UniqueConstraint(columnNames = {"NAME"}))
public class Product {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "NAME")
    private String name;

    @Column(name = "AGE_MIN")
    private Integer ageMin;

    @Column(name = "AGE_MAX")
    private Integer ageMax;

    @Column(name = "STUDENT")
    private Boolean isStudent;

    @Column(name = "INCOME_MIN")
    private Integer incomeMin;

    @Column(name = "INCOME_MAX")
    private Integer incomeMax;

    public Product() {
        super();
    }

    public Product(Long id, String name, Integer ageMin, Integer ageMax, Boolean isStudent, Integer incomeMin, Integer incomeMax) {
        this.id = id;
        this.name = name;
        this.ageMin = ageMin;
        this.ageMax = ageMax;
        this.isStudent = isStudent;
        this.incomeMin = incomeMin;
        this.incomeMax = incomeMax;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAgeMin(Integer ageMin) {
        this.ageMin = ageMin;
    }

    public void setAgeMax(Integer ageMax) {
        this.ageMax = ageMax;
    }

    public void setIsStudent(Boolean isStudent) {
        this.isStudent = isStudent;
    }

    public void setIncomeMin(Integer incomeMin) {
        this.incomeMin = incomeMin;
    }

    public void setIncomeMax(Integer incomeMax) {
        this.incomeMax = incomeMax;
    }
}
