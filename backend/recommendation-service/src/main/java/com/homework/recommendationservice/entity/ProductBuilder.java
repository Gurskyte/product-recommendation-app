package com.homework.recommendationservice.entity;

public class ProductBuilder {
    private Long id;
    private String name;
    private Integer ageMin;
    private Integer ageMax;
    private Boolean isStudent;
    private Integer incomeMin;
    private Integer incomeMax;

    public ProductBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public ProductBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public ProductBuilder withAgeMin(Integer ageMin) {
        this.ageMin = ageMin;
        return this;
    }

    public ProductBuilder withAgeMax(Integer ageMax) {
        this.ageMax = ageMax;
        return this;
    }

    public ProductBuilder isStudent(Boolean isStudent) {
        this.isStudent = isStudent;
        return this;
    }

    public ProductBuilder withIncomeMin(Integer incomeMin) {
        this.incomeMin = incomeMin;
        return this;
    }

    public ProductBuilder withIncomeMax(Integer incomeMax) {
        this.incomeMax = incomeMax;
        return this;
    }

    public Product build() {
        return new Product(this.id, this.name, this.ageMin, this.ageMax, this.isStudent, this.incomeMin, this.incomeMax);
    }

    public static ProductBuilder product() {
        return new ProductBuilder();
    }
}