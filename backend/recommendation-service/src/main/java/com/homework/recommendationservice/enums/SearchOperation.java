package com.homework.recommendationservice.enums;

public enum SearchOperation {
    EQUALITY,
    GREATER_THAN,
    LESS_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN_OR_EQUAL;

    public static final String[] SIMPLE_OPERATION_SET = {":", ">=", "<=", ">", "<"};

    public static SearchOperation getSimpleOperation(String input) {
        switch (input) {
            case ":":
                return EQUALITY;
            case ">":
                return GREATER_THAN;
            case "<":
                return LESS_THAN;
            case ">=":
                return GREATER_THAN_OR_EQUAL;
            case "<=":
                return LESS_THAN_OR_EQUAL;
            default:
                return null;
        }
    }
}