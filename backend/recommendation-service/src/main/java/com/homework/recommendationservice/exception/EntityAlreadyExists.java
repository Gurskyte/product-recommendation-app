package com.homework.recommendationservice.exception;

public class EntityAlreadyExists extends Exception{
    public EntityAlreadyExists(String message, Throwable cause) {
        super(message, cause);
    }
}
