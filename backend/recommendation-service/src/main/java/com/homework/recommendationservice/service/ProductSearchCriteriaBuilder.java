package com.homework.recommendationservice.service;

import com.homework.recommendationservice.enums.SearchOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductSearchCriteriaBuilder {

    List<SearchCriteria> searchCriteriaList;
    String modifiedCondition;

    public List<SearchCriteria> build(String query) {
        if (query == null || query.isBlank()) {
            return new ArrayList<>();
        }
        String[] splittedConditions = query.split(",");
        searchCriteriaList = new ArrayList<>();
        for (String condition : splittedConditions) {
            SearchOperation searchOperation = getOperationSymbol(condition);
            modifiedCondition = replaceOperation(condition, searchOperation);
            createSearchCriteriaList(modifiedCondition, searchOperation);
        }
        return searchCriteriaList;
    }

    private String replaceOperation(String condition, SearchOperation searchOperation) {
        int operationIndex = StringUtils.indexOfAny(condition, SearchOperation.SIMPLE_OPERATION_SET);
        if (isTwoSymbolOperation(searchOperation)) {
            return replaceCondition(condition, searchOperation, operationIndex, 2);
        }
        return replaceCondition(condition, searchOperation, operationIndex, 1);
    }

    private String replaceCondition(String condition, SearchOperation searchOperation, int operationIndex, int operationSymbolLength) {
        return condition.replaceAll(condition.substring(operationIndex, operationIndex + operationSymbolLength), searchOperation.name());
    }

    private boolean isTwoSymbolOperation(SearchOperation searchOperation) {
        return searchOperation.equals(SearchOperation.GREATER_THAN_OR_EQUAL) || searchOperation.equals(SearchOperation.LESS_THAN_OR_EQUAL);
    }

    private void createSearchCriteriaList(String condition, SearchOperation operationSymbol) {
        String[] splittedCondition = condition.split(operationSymbol.name());
        String searchCriteriaKey = splittedCondition[0];
        String value = splittedCondition[1];
        searchCriteriaList.add(new SearchCriteria(searchCriteriaKey, operationSymbol, value));
    }

    private SearchOperation getOperationSymbol(String condition) {
        Optional<String> operation = Arrays.stream(SearchOperation.SIMPLE_OPERATION_SET).filter(condition::contains).findAny();
        return SearchOperation.getSimpleOperation(operation.orElse("Operation not found in condition: " + condition));
    }
}
