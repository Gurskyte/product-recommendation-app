package com.homework.recommendationservice.service;

import com.homework.recommendationservice.entity.Product;
import com.homework.recommendationservice.enums.SearchOperation;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.function.Consumer;

public class ProductSearchQueryCriteriaConsumer implements Consumer<SearchCriteria> {
    private Predicate predicate;
    private final CriteriaBuilder builder;
    private final Root<Product> productRoot;

    public ProductSearchQueryCriteriaConsumer(Predicate predicate, CriteriaBuilder builder, Root<Product> productRoot) {
        this.predicate = predicate;
        this.builder = builder;
        this.productRoot = productRoot;
    }

    @Override
    public void accept(SearchCriteria searchCriteria) {
        String key = searchCriteria.getKey();
        Object value = searchCriteria.getValue();
        SearchOperation operation = searchCriteria.getOperation();
        if (value.equals("null")) {
            predicate = builder.isNull(productRoot.get(key));
        } else if (key.equals("isStudent")) {
            value = Boolean.valueOf((String) value);
            predicate = builder.or(builder.equal(productRoot.get(key), value), builder.isNull(productRoot.get(key)));
        } else {
            switch (operation) {
                case EQUALITY:
                    predicate = builder.or(builder.equal(productRoot.get(key), String.valueOf(value)), builder.isNull(productRoot.get(key)));
                    break;
                case LESS_THAN:
                    predicate = builder.or(builder.lessThan(productRoot.get(key), String.valueOf(value)));
                    break;
                case GREATER_THAN_OR_EQUAL:
                    predicate = builder.or(builder.greaterThanOrEqualTo(productRoot.get(key), String.valueOf(value)), builder.isNull(productRoot.get(key)));
                    break;
                case LESS_THAN_OR_EQUAL:
                    predicate = builder.or(builder.lessThanOrEqualTo(productRoot.get(key), String.valueOf(value)), builder.isNull(productRoot.get(key)));
                    break;
                case GREATER_THAN:
                    predicate = builder.or(builder.greaterThan(productRoot.get(key), String.valueOf(value)), builder.isNull(productRoot.get(key)));
                    break;
                default:
                    predicate = null;
            }
        }
    }

    public Predicate getPredicate() {
        return predicate;
    }
}