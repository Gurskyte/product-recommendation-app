package com.homework.recommendationservice.service;

import com.homework.recommendationservice.dao.ProductDao;
import com.homework.recommendationservice.entity.Product;
import com.homework.recommendationservice.exception.EntityAlreadyExists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductDao productDao;

    public List<Product> getProducts(List<SearchCriteria> searchCriteriaList) {
        return productDao.searchProduct(searchCriteriaList);
    }

    public Product saveProduct(Product product) throws EntityAlreadyExists {
        return productDao.saveProduct(product);
    }
}
