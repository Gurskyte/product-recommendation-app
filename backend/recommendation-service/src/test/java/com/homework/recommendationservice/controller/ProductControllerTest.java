package com.homework.recommendationservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.homework.recommendationservice.entity.Product;
import com.homework.recommendationservice.exception.EntityAlreadyExists;
import com.homework.recommendationservice.service.ProductSearchCriteriaBuilder;
import com.homework.recommendationservice.service.ProductService;
import com.homework.recommendationservice.service.SearchCriteria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManagerFactory;
import java.util.List;

import static com.homework.recommendationservice.entity.ProductBuilder.product;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

    public static final String VISA = "visa";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private ProductSearchCriteriaBuilder searchCriteriaBuilder;

    @MockBean
    private EntityManagerFactory entityManagerFactory;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void givenNoProducts_whenGetProducts_thenReturnStatusOk() throws Exception {
        mvc.perform(get("/api/products")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void givenNoProducts_whenGetProducts_thenReturnEmptyProductsList() throws Exception {
        mvc.perform(get("/api/products")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.products", is(emptyList())));
    }

    @Test
    public void givenProducts_whenGetProducts_thenReturnAllProducts() throws Exception {
        Product product = product().build();
        List<SearchCriteria> searchCriteriaList = emptyList();

        when(productService.getProducts(searchCriteriaList)).thenReturn(List.of(product));

        mvc.perform(get("/api/products")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.products", hasSize(1)));
    }

    @Test
    public void givenProducts_whenGetProductsWithAgeParameter_thenReturnAllProducts() throws Exception {
        Product product = product().build();
        List<SearchCriteria> searchCriteriaList = emptyList();

        when(productService.getProducts(searchCriteriaList)).thenReturn(List.of(product));

        mvc.perform(get("/api/products")
                .param("search", "ageMin:0", "ageMax:17")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.products", hasSize(1)));
    }

    @Test
    public void givenProducts_whenPostProduct_thenStatusCreated() throws Exception {
        Product product = product().withName(VISA).build();

        when(productService.saveProduct(product)).thenReturn(product);

        mvc.perform(post("/api/product")
                .content(objectMapper.writeValueAsString(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(objectMapper.writeValueAsString(product)));
    }

    @Test
    public void givenProducts_whenPostProduct_thenStatusConflict() throws Exception {
        Product product = product().withName(VISA).build();

        when(productService.saveProduct(product)).thenThrow(EntityAlreadyExists.class);

        mvc.perform(post("/api/product")
                .content(objectMapper.writeValueAsString(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andExpect(content().string("Product already exists."));
    }

    @Test
    public void givenProductWithId_whenPostProduct_thenIdIsIgnored() throws Exception {
        Product product = product().withId(1L).withName(VISA).build();
        Product expectedProduct = product().withName(VISA).build();

        when(productService.saveProduct(expectedProduct)).thenReturn(product);

        mvc.perform(post("/api/product")
                .content(objectMapper.writeValueAsString(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(objectMapper.writeValueAsString(product)));
    }

    @Test
    public void givenProductWithoutName_whenPostProduct_thenBadRequest() throws Exception {
        Product product = product().build();

        mvc.perform(post("/api/product")
                .content(objectMapper.writeValueAsString(product))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}