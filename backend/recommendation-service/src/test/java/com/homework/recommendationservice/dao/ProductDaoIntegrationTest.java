package com.homework.recommendationservice.dao;

import com.homework.recommendationservice.entity.Product;
import com.homework.recommendationservice.exception.EntityAlreadyExists;
import com.homework.recommendationservice.service.SearchCriteria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

import static com.homework.recommendationservice.entity.ProductBuilder.product;
import static com.homework.recommendationservice.enums.SearchOperation.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductDaoIntegrationTest {

    @Autowired
    private ProductDao productDao;

    @PersistenceContext
    private EntityManager entityManager;

    public static final String VISA = "visa";
    public static final String MASTERCARD = "mastercard";
    public static final String AMERICAN_EXPRESS = "americanExpress";

    private final List<SearchCriteria> searchCriteriaList = new ArrayList<>();

    @Test
    public void givenProducts_whenNoSearchCriteria_thenResultContainsAllProducts() throws EntityAlreadyExists {
        Product visa = product().withName(VISA).build();
        Product mastercard = product().withName(MASTERCARD).build();

        productDao.saveProduct(visa);
        productDao.saveProduct(mastercard);

        List<Product> productList = productDao.searchProduct(searchCriteriaList);
        assertEquals(2, productList.size());
        assertTrue(productIsInList(productList, VISA));
        assertTrue(productIsInList(productList, MASTERCARD));
    }

    @Test
    public void givenProducts_whenStudentEqualityTrueSearchCriteria_ThenResultContainsOnlyProductsWithStudentTrueValue() throws EntityAlreadyExists {
        Product visa = product().withName(VISA).isStudent(true).build();
        Product mastercard = product().withName(MASTERCARD).isStudent(false).build();

        productDao.saveProduct(visa);
        productDao.saveProduct(mastercard);

        searchCriteriaList.add(new SearchCriteria("isStudent", EQUALITY, "true"));

        List<Product> productList = productDao.searchProduct(searchCriteriaList);

        assertEquals(1, productList.size());
        assertTrue(productIsInList(productList, VISA));
        assertFalse(productIsInList(productList, MASTERCARD));
    }

    @Test
    public void givenProducts_whenStudentEqualityFalseSearchCriteria_ThenResultContainsOnlyProductsWithStudentFalseOrNullValue() throws EntityAlreadyExists {
        Product visa = product().withName(VISA).isStudent(true).build();
        Product mastercard = product().withName(MASTERCARD).isStudent(false).build();
        Product americanExpress = product().withName(AMERICAN_EXPRESS).build();

        productDao.saveProduct(visa);
        productDao.saveProduct(mastercard);
        productDao.saveProduct(americanExpress);

        searchCriteriaList.add(new SearchCriteria("isStudent", EQUALITY, "true"));

        List<Product> productList = productDao.searchProduct(searchCriteriaList);

        assertEquals(2, productList.size());
        assertTrue(productIsInList(productList, VISA));
        assertTrue(productIsInList(productList, AMERICAN_EXPRESS));
        assertFalse(productIsInList(productList, MASTERCARD));
    }

    @Test
    public void givenProducts_whenMultipleIncomeCriteria_thenResultContainsProductsByCriteria() throws EntityAlreadyExists {
        Product visa = product().withName(VISA).withIncomeMin(0).build();
        Product mastercard = product().withName(MASTERCARD).withIncomeMin(1).withIncomeMax(12000).build();
        Product americanExpress = product().withName(AMERICAN_EXPRESS).withIncomeMin(12001).withIncomeMax(40000).build();

        productDao.saveProduct(visa);
        productDao.saveProduct(mastercard);
        productDao.saveProduct(americanExpress);

        searchCriteriaList.add(new SearchCriteria("incomeMin", LESS_THAN_OR_EQUAL, "12000"));
        searchCriteriaList.add(new SearchCriteria("incomeMax", GREATER_THAN, "1"));

        List<Product> productList = productDao.searchProduct(searchCriteriaList);

        assertEquals(2, productList.size());
        assertTrue(productIsInList(productList, VISA));
        assertTrue(productIsInList(productList, MASTERCARD));
        assertFalse(productIsInList(productList, AMERICAN_EXPRESS));
    }

    @Test
    public void givenProducts_whenMultipleCriteria_thenResultContainsProductByAllCriteria() throws EntityAlreadyExists {
        Product visa = product().withName(VISA).withAgeMin(0).withIncomeMin(12001).build();
        Product mastercard = product().withName(MASTERCARD).withAgeMin(18).withIncomeMin(40001).build();

        productDao.saveProduct(visa);
        productDao.saveProduct(mastercard);

        searchCriteriaList.add(new SearchCriteria("ageMin", LESS_THAN, "17"));
        searchCriteriaList.add(new SearchCriteria("incomeMin", GREATER_THAN, "12000"));

        List<Product> productList = productDao.searchProduct(searchCriteriaList);

        assertEquals(1, productList.size());
        assertTrue(productIsInList(productList, VISA));
        assertFalse(productIsInList(productList, MASTERCARD));
    }

    @Test
    public void givenProduct_whenSaveProduct_thenProductIsSaved() throws EntityAlreadyExists {
        Product visa = product().withName(VISA).withAgeMin(0).withAgeMax(64).isStudent(true).withIncomeMin(12001).withIncomeMax(40000).build();

        Product savedProduct = productDao.saveProduct(visa);

        assertEquals(savedProduct, visa);
        assertTrue(entityManager.contains(visa));
    }

    @Test(expected = EntityAlreadyExists.class)
    public void givenExistingProduct_whenSaveProduct_ResponseContainsMessage() throws EntityAlreadyExists {
        Product product1 = product().withName(VISA).build();
        Product product2 = product().withName(VISA).build();

        productDao.saveProduct(product1);
        productDao.saveProduct(product2);
    }

    private boolean productIsInList(List<Product> productList, String productName) {
        return productList.stream().anyMatch(product -> product.getName().contains(productName));
    }
}