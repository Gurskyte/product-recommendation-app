package com.homework.recommendationservice.service;

import com.homework.recommendationservice.enums.SearchOperation;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ProductSearchCriteriaBuilderTest {

    @Test
    public void givenSearchValue_whenBuildSearchCriteria_thenSearchCriteriaListIsReturned() {
        ProductSearchCriteriaBuilder criteriaBuilder = new ProductSearchCriteriaBuilder();
        String searchValue = "ageMin:0,ageMax:17";
        List<SearchCriteria> searchCriteriaList = criteriaBuilder.build(searchValue);
        assertEquals(2, searchCriteriaList.size());
    }

    @Test
    public void givenSearchValue_whenBuildSearchCriteria_thenCorrectSearchCriteriaValues() {
        ProductSearchCriteriaBuilder criteriaBuilder = new ProductSearchCriteriaBuilder();
        String searchValue = "ageMin>0,student:true,incomeMax<1000";
        List<SearchCriteria> searchCriteriaList = criteriaBuilder.build(searchValue);

        assertEquals(3, searchCriteriaList.size());
        SearchCriteria ageCriteria = searchCriteriaList.get(0);
        SearchCriteria studentCriteria = searchCriteriaList.get(1);
        SearchCriteria incomeCriteria = searchCriteriaList.get(2);

        assertEquals("ageMin", ageCriteria.getKey());
        assertEquals("student", studentCriteria.getKey());
        assertEquals("incomeMax", incomeCriteria.getKey());

        Assert.assertEquals(SearchOperation.GREATER_THAN, ageCriteria.getOperation());
        Assert.assertEquals(SearchOperation.EQUALITY, studentCriteria.getOperation());
        Assert.assertEquals(SearchOperation.LESS_THAN, incomeCriteria.getOperation());

        assertEquals("0", ageCriteria.getValue());
        assertEquals("true", studentCriteria.getValue());
        assertEquals("1000", incomeCriteria.getValue());
    }

    @Test
    public void givenEmptySearchValue_whenBuildSearchCriteria_thenEmptyListIsReturned() {
        ProductSearchCriteriaBuilder criteriaBuilder = new ProductSearchCriteriaBuilder();

        assertEquals(Collections.emptyList(), criteriaBuilder.build(""));
    }
}