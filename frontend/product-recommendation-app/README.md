# ProductRecommendationApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

## Development server

Run `npm update` and then `npm start` of `ng serve` commands in project root directory for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## TODO

- Refresh page after submit new product.
- Validate that age and income min values would not be higher than max values.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
