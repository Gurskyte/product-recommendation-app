import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Product } from './products/products.component';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private httpClient: HttpClient) {}

  getRecommendedProducts(
    ageMin: string,
    ageMax: string,
    student: string,
    incomeMin: string,
    incomeMax: string
  ): Observable<{ products: Product[] }> {
    let url = 'http://localhost:8080/api/products';
    let params = new HttpParams();

    const query =
      this.buildAgeQuery(ageMin, ageMax) +
      this.buildIncomeQuery(incomeMin, incomeMax) +
      this.buildStudentQuery(student);

    params = params.append('search', query);
    return this.httpClient.get<{ products: Product[] }>(url, { params });
  }

  buildAgeQuery(ageMin: string, ageMax: string) {
    let junior = '0-17';
    let adult = '18-64';
    let senior = '65';

    let ageRange = ageMin + (ageMax ? '-' + ageMax : '');

    switch (ageRange) {
      case junior:
        return `ageMin<=${ageMin},ageMax<=${ageMax},`;
      case adult:
        return `ageMin<=${ageMax},ageMax>=${ageMin},`;
      case senior:
        return `ageMin>=${ageMin},`;
      default:
        return '';
    }
  }

  buildStudentQuery(student: string) {
    switch (student) {
      case 'true':
      case 'false':
        return `isStudent:${student},`;
      default:
        return '';
    }
  }

  buildIncomeQuery(incomeMin: string, incomeMax: string) {
    let zeroIncome = '0';
    let lowIncome = '1-12000';
    let averageIncome = '12001-40000';
    let highIncome = '40001';

    let incomeRange = incomeMin + (incomeMax ? '-' + incomeMax : '');

    switch (incomeRange) {
      case zeroIncome:
        return `incomeMin<=${incomeMin},`;
      case lowIncome:
      case averageIncome:
        return `incomeMin<=${incomeMax},incomeMax>${incomeMin},`;
      case highIncome:
        return `incomeMax>=${incomeMin},`;
      default:
        return '';
    }
  }

  postProduct(product: Product): Observable<Product> {
    let url = 'http://localhost:8080/api/product';
    return this.httpClient.post<Product>(url, product);
  }
}
