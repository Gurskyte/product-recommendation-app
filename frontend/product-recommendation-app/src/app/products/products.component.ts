import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/data.service';
import { FormBuilder } from '@angular/forms';

export interface Product {
  name: string;
  ageMin: string;
  ageMax: string;
  isStudent: string;
  incomeMin: string;
  incomeMax: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  products = [];
  product = null;
  name = null;
  ageMin = null;
  ageMax = null;
  student = null;
  incomeMin = null;
  incomeMax = null;
  products$: Observable<{ products: Product[] }>;
  product$: Observable<{ product: Product }>;
  checkoutForm = this.formBuilder.group({
    name: '',
    ageMin: '',
    ageMax: '',
    isStudent: '',
    incomeMin: '',
    incomeMax: '',
  });

  constructor(
    private dataservice: DataService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getProductsUsingAsyncPipe();
  }

  public getProductsUsingAsyncPipe() {
    this.dataservice
      .getRecommendedProducts(
        this.ageMin,
        this.ageMax,
        this.student,
        this.incomeMin,
        this.incomeMax
      )
      .subscribe((response) => {
        this.products = response.products;
      });
  }

  public selectAgeRange(range: string) {
    const [ageMin, ageMax] = range.split('-');
    this.ageMin = ageMin;
    this.ageMax = ageMax;
    this.getProductsUsingAsyncPipe();
  }

  public selectIsStudent(value: string) {
    this.student = value;
    this.getProductsUsingAsyncPipe();
  }

  public selectIncomeRange(range: string) {
    const [incomeMin, incomeMax] = range.split('-');
    this.incomeMin = incomeMin;
    this.incomeMax = incomeMax;
    this.getProductsUsingAsyncPipe();
  }

  public postProduct() {
    this.dataservice
      .postProduct(this.checkoutForm.value)
      .subscribe((response) => {
        this.product = response;
      });
  }
}
